#!/usr/bin/env python3
# ------------------------------------------------------------------------------

def addition(x,y):
    return x + y

def subtraction(x,y):
    return x - y

def multiplication(x,y):
    return x * y


def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = ' ** '
  print('%s got: %s expected: %s' % (prefix, repr(got), repr(expected)))

if __name__ == '__main__':
  print("Testing:")
  test(addition(3,3),6)


